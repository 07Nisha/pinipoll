import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Splash from '../screens/commonClasses/auth/splash';
import SelectAccount from '../screens/commonClasses/auth/selectAccount';
import PersonalRegister from '../screens/commonClasses/auth/register/personalRegister';
import EnterPin from '../screens/commonClasses/auth/enterPin';
import ForgotPassword from '../screens/commonClasses/auth/forgotPassword';
import LoginScreen from '../screens/commonClasses/auth/login';
import SubscriptionPlan from '../screens/commonClasses/subscriptionPlan';
import ResetPassword from '../screens/commonClasses/auth/resetPassword';
import BusinessRegister from '../screens/commonClasses/auth/register/business/businessRegister';
import AboutBusiness from '../screens/commonClasses/auth/register/business/aboutBusiness';
import PersonalHome from '../screens/personal/personalHome';
import TermsScreen from '../screens/commonClasses/auth/termsAndConditions';
import Animated from 'react-native-reanimated';
import CustomDrawerContent from '../screens/drawer/CustomeDrawerContentComponent';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLORS from '../utils/Colors';
import CreateAPoll from '../screens/personal/createAPoll';
import Categories from '../screens/commonComponents/Categories';
import PollSettings from '../screens/personal/pollSettings';
import AddPollImages from '../screens/commonComponents/AddPollImages';
import PersonalPolls from '../screens/personal/personalHome/feed';
import CreateOrgPoll from '../screens/organization/createPoll';
import PersonalEditProfile from '../screens/Profile/PersonalProfile/PerEditProfile';
import PersonalProfileScreen from '../screens/Profile/PersonalProfile/PerProfileScreen';
import AddMoreDetails from '../screens/Profile/PersonalProfile/AddMoreDetails';
import ChooseInterests from '../screens/Profile/PersonalProfile/ChooseInterests';
import BusinessEditProfile from '../screens/Profile/BusinessProfile/BusEditProfile';
import BusinessAddMoreDetails from '../screens/Profile/BusinessProfile/BusAddMoreDetails';
import BusinessProfileScreen from '../screens/Profile/BusinessProfile/BusProfileScreen';
import PopularPoll from '../screens/popularPoll';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


const DrawerNavigator = () => {
    return (
        <Drawer.Navigator initialRouteName="Home"
                          drawerStyle={{
                              width: wp(78),
                              backgroundColor: COLORS.backGround_color,
                          }}
                          drawerContent={props => {
                              return <CustomDrawerContent {...props} />;
                          }}>
            <Drawer.Screen name="Screens" component={DrawerScreens}/>
        </Drawer.Navigator>
    );
};

const AppNavigator = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={'splash'} headerMode="none">
                <Stack.Screen name="splash" component={Splash}/>
                <Stack.Screen name="selectAccount" component={SelectAccount}/>
                <Stack.Screen name="register" component={PersonalRegister}/>
                <Stack.Screen name="enterPin" component={EnterPin}/>
                <Stack.Screen name="login" component={LoginScreen}/>
                <Stack.Screen name="forgotPassword" component={ForgotPassword}/>

                <Stack.Screen name="resetPassword" component={ResetPassword}/>
                <Stack.Screen name="subscriptionPlan" component={SubscriptionPlan}/>
                <Stack.Screen name="businessSignup" component={BusinessRegister}/>
                <Stack.Screen name="tellAboutBusiness" component={AboutBusiness}/>

                <Stack.Screen name="personalHome" component={DrawerNavigator}/>
                <Stack.Screen name="terms" component={TermsScreen}/>
                <Stack.Screen name="pCreatePoll" component={CreateAPoll}/>
                <Stack.Screen name="pollCategories" component={Categories}/>
                <Stack.Screen name="pollSettings" component={PollSettings}/>
                <Stack.Screen name="addPollImages" component={AddPollImages}/>

                <Stack.Screen name="orgCreatePoll" component={CreateOrgPoll}/>

                <Stack.Screen name="personalProfileScreen" component={PersonalProfileScreen} />
                <Stack.Screen name="personalEditProfile" component={PersonalEditProfile} />
                {/*<Stack.Screen name="personalHome" component={DrawerNavigator}/>*/}
                {/*<Stack.Screen name="pCreatePoll" component={CreateAPoll}/>*/}

                <Stack.Screen name="AddMoreDetails" component={AddMoreDetails} />
                <Stack.Screen name="ChooseInterests" component={ChooseInterests} />

                <Stack.Screen name="businessProfileScreen" component={BusinessProfileScreen} />

                <Stack.Screen name="businessEditProfile" component={BusinessEditProfile} />
                <Stack.Screen name="businessAddMoreDetails" component={BusinessAddMoreDetails} />
                <Stack.Screen name="popularHome"  component={PopularPoll}/>

            </Stack.Navigator>
        </NavigationContainer>
    );
};


const DrawerScreens = ({style}) => {
    return (
        <Animated.View style={[{flex: 1, overflow: 'hidden'}, style]}>
            <Stack.Navigator initialRouteName="pHome" headerMode="none">
                <Stack.Screen name="pHome" component={PersonalHome}/>
                <Stack.Screen name="pCreatePoll" component={CreateAPoll}/>
            </Stack.Navigator>
        </Animated.View>
    );
};

export default AppNavigator;
