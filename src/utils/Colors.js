const COLORS = {
    app_theme_color: '#92278F',
    app_theme_color_disabled: 'rgba(146, 39, 143, 0.5)',
    text_Color: '#363639',

    off_black: '#363639',
    // backGround_color: '#E5E5E5',
    backGround_color: '#ffffff',
    white_color: '#ffffff',
    btn_color: '#FFC20F',
    black_color: '#000000',
    light_grey: '#E2E2E2',
    light_grey1: '#E9E9E9',
    grey_color: '#828282',
    valid_Color: '#239235',
    invalid_color: '#FF0000',
    failure_Toast: '#EA4242',
    modal_color: 'rgba(0,0,0,0.3)',
    placeHolder_color: 'rgb(136,148,159)',
    arrow_color: 'rgb(189,189,189)',
    image_background_color:'#C4C4C4',
    poll_voted_color:'#27AE60',
    button_Light_color:"#E0E0E0",

};
export default COLORS;
