const ICONS = {
    LOGO: require('../assets/images/Logo.png'),
    INSTAGRAM: require('../assets/images/Instagram.png'),
    PLUS_ICON:require('../assets/images/PlusIcon.png'),
    CROSS_ICON:require('../assets/images/Cross.png'),
    EDIT_ICON:require('../assets/images/Edit.png'),
}

export {
    ICONS
};
