import * as _ from 'lodash';

const TransformDropdownData = (data) => {
    let array = [];
    data.map(item => {
        array.push({
            value: item.id,
            label: item.name,
        });
    });
    return array;
};

const TransformCountryData = (data) => {
    let array = [];
    let res = _.uniqBy(data, 'country');
    res.map(item => {
        array.push({
            value: item.id,
            label: item.country,
        });
    });
    return array;
};

const TransformCityData = (data) => {
    let array = [];
    let res = _.uniqBy(data, 'admin_name');
    res.map(item => {
        array.push({
            value: item.id,
            label: `${item.city}-${item.admin_name}`,
        });
    });
    return array;
};

const TransformOccupationData = (data) => {
    let array = [];
    data.map(item => {
        array.push({
            value: item.id,
            label: item.Occuption_ame,
        });
    });
    return array;
};

const TransformReligionData = (data) => {
    let array = [];
    data.map(item => {
        array.push({
            value: item.id,
            label: item.Religion_name,
        });
    });
    return array;
};

export {
    TransformDropdownData,
    TransformCountryData,
    TransformCityData,
    TransformOccupationData,
    TransformReligionData
};
