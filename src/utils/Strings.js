import React from 'react';

const STRINGS = {
    // ==============================================
    // ================ASYNC - STORAGE===============
    // ==============================================

    accessToken: 'ACCESS_TOKEN',
    email: 'EMAIL',
    loginToken: 'LOGIN_TOKEN',
    loginData: 'LOGIN_DATA',
    userPersonalInfo:'PERSONAL_INFO',

    //==============================================
    CHECK_INTERNET: 'Please check your internet connection',
    COMING_SOON: 'COMING SOON',
    APP_NAME: 'PINIPOL',
    SELECT_IMAGE: 'Select image to upload.',
    GALLERY_TEXT: 'Choose from Gallery',
    CAMERA_TEXT: 'Choose from Camera',
    DELETE_UPLOAD_TEXT: 'Do you want to delete upload image.',
    YES_SURE_TEXT: 'Yes Sure',
    CANCEL_TEXT: 'Cancel',
    LOADING_TEXT: 'Loading..',
    EXIT_ALERT: 'Are you sure you want to logout from the application?',
    //==============================================
    //================Login Page====================
    //==============================================
    login_text: 'Login',
    email_placeholder: 'Email/Mobile Number',
    password_placeholder: 'Password',
    forgot_text: 'Forgot Password?',
    login_button: 'Login',
    or_text: 'OR',
    dont_have_text: 'Don not have an account?',
    register_text: 'REGISTER',
    USER_TEXT: 'Username',

    //=====================================================
    //================Forgot Password======================
    //=====================================================
    forgot_header_title: 'Forgot Password',
    recover_password: 'Recover Password',
    enter_register: 'Please Enter Registered Email Id,\n A temporary Password will be sent \n to registered Email Id.',
    enter_email: 'Enter Your Email ID',
    send_button: 'Send',

    //==========================================================
    //================Customer Register Page====================
    //==========================================================
    select_type: 'Select Type',
    reg_header_title: 'Register',
    reg_first_name_placeholder: 'First Name',
    reg_last_name_placeholder: 'Last Name',
    reg_email_placeholder: 'Email',
    limit_exceeded_text: 'Limit exceeded. Please try after some time.',
    unauthorized_phone_number: 'Invalid phone number',
    //=====================================================
    //================Register Page====================
    //=====================================================

    reg_phone_number_placeholder: 'Phone Number',
    reg_city_placeholder: 'City',
    reg_state_placeholder: 'State',
    reg_country_placeholder: 'Country',
    reg_agree_tc: 'Agree to terms and conditions',
    reg_submit_button: 'Submit',
    reg_password_placeholder: 'Enter a Password',
    reg_confirm_password_placeholder: 'Confirm Password',
    MOBILE_PLACEHOLDER_TEXT1: '1',
    MOBILE_PLACEHOLDER_TEXT: 'Phone Number',
    MOBILE_NUMBER: 'Phone Number',
    SEARCH_TEXT: 'search',
    TERM_CONDITION_TEXT: 'Agree to Terms and Conditions',

    // ----------------------------------------
    // VALIDATION OR API ERROR/SUCCESS MESSAGES
    // ----------------------------------------
    username_email: 'Email field can not be empty.',
    name_minimum_three: 'Name can not be less then 3 letters',
    empty_password: 'Password field can not be empty.',
    empty_confirm_password: 'Confirm Password field can not be empty.',
    password_does_not_match: 'Password and Confirm Password does not match.',
    valid_email: 'Please enter a valid email.',
    empty_first_name: 'First name field can not be empty.',
    empty_last_name: 'Last name field can not be empty.',
    empty_city_name: 'City name field cannot be empty.',
    empty_state_name: 'State name field cannot be empty.',
    empty_country_name: 'Country name field can not be empty.',
    enter_valid_password: 'Password should be strong! Min 8 characters required including 1 upper, lowercase, 1 special & 1 numeric character.',
    ONLY_TEN_IMAGES: 'You can select maximum ten images.',
    enter_mobileNo: 'Mobile number can not be empty',
    termAndConditionFill: 'Please select terms and conditions',
    //============== Register =============================
    empty_owner_name: 'Owner first name field can not be empty.',
    empty_owner_hotel_name: 'Hotel name field can not be empty.',
    empty_owner_hotel_address: 'Hotel address field can not be empty.',
    empty_owner_phone_number: 'Phone number field can not be empty.',
    empty_owner_hotel_detail: 'Hotel detail field can not be empty.',

    //======================= Select Account ==========================

    select_Account: 'Select Account Type',
    select_account_text: 'Get started by choosing your account type below',

    //========================= Register ===============================

    create_account_text: 'Create Personal Account',
    register_btn_text: 'Agree and Create Account',

    //========================= Forgot Password ===============================

    forgot_password: 'Please enter your username.\n We will send you a PIN to your registered email Id \n to reset your password.',
    send_email: 'Send Email',
    back_to_login: 'Back to login',


    //========================= Enter PIn ===============================

    enterPin: 'Enter PIN',
    detail_text_pin: 'We have sent you a PIN to your email address. Please enter the PIN below.',
    enter_valid_pin: 'Enter valid PIN.',
    verify_btn: 'Verify',
    resend_text: 'Resend PIN',

    //========================= Reset Password ===============================

    confirm_Text: 'Confirm',
    create_Password: 'Create new password',
    continue_text: 'Continue',

    //=====================================================
    //================BusinessSignup Page===================
    //=====================================================
    BS_CREATE_BUSINESS_TEXT: 'Create \n Business Account',
    BS_ORGANISATION_NAME_PLACEHOLDER: 'Organisation Name',
    BS_CREATE_EMAIL_ID_PLACEHOLDER: 'Email ID',
    BS_CREATE_PASSWORD_PLACEHOLDER: 'Create Password',
    BS_CONFIRM_PASSWORD_PLACEHOLDER: 'Confirm Password',
    BS_NEXT_TEXT: 'Next',
    BS_ALREADY_HAVE_TEXT: 'Already have an account',
    BS_LOGIN_TEXT: 'Login',

    //=======================================================
    //================About Business Page====================
    //=======================================================
    ABOUT_BUSINESS_TELL_US_TEXT: 'Tell us about your \n business',
    ABOUT_BUSINESS_CONTACT_TEXT: 'Business Contact',
    ABOUT_BUSINESS_LEGAL_FIRST_PLACEHOLDER: 'Legal First Name',
    ABOUT_BUSINESS_LEGAL_LAST_PLACEHOLDER: 'Legal Last Name',
    ABOUT_BUSINESS_POSITION_PLACEHOLDER: 'Position',
    ABOUT_BUSINESS_DETAIL_TEXT: 'Business Details',
    ABOUT_BUSINESS_ORGANISATION_TYPE_PLACEHOLDER: 'Organisation Type',
    ABOUT_BUSINESS_SECTOR_PLACEHOLDER: 'Sector',
    ABOUT_BUSINESS_PHONE_NUMBER_TEXT: 'Business Phone Number',
    ABOUT_BUSINESS_PHONE_NO_PLACEHODER: 'Phone no.',
    ABOUT_BUSINESS_WEBSITE_PLACEHOLDER: 'Website',
    ABOUT_BUSINESS_ADDRESS_TEXT: 'Business Address',
    ABOUT_BUSINESS_STREET_ADDRESS_PLACEHOLDER: 'Street Address',
    ABOUT_BUSINESS_CITY_PLACEHOLDER: 'City',
    ABOUT_BUSINESS_COUNTRY_PLACEHOLDER: 'Country',
    ABOUT_BUSINESS_AGREE_CREATE_TEXT: 'Agree and Create Account',
    ABOUT_BUSINESS_ZIP_PLACEHOLDER: 'Zip Code',
    ABOUT_BUSINESS_ACCEPT_TEXT: 'I accept the ',
    ABOUT_BUSINESS_TERM_AND_TEXT: 'terms and conditions',

    //=======================================================
    //================Subscription Plan Page====================
    //=======================================================
    SUBS_PLAN_TITLE_TEXT: 'Choose a Subscription Plan',
    limited_period:"FREE for limited period!",
    initial_text:"All app functions are currently available without charge for a limited period. In-App purchases for some of the current or additional functions will be launched in the future.",

    //=======================================================
    //================Terms and conditions====================
    //=======================================================
    TERMS_CONDITIONS_TEXT: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer velit mi, iaculis ac mollis in, mattis a quam. Curabitur mollis mattis ullamcorper. Aenean laoreet vulputate condimentum. Fusce ut massa purus. Morbi cursus ultrices interdum. Sed volutpat sem eget dui posuere ultricies. Phasellus viverra, mi vel volutpat pulvinar, nisl elit ultricies metus, in ornare lectus eros eget augue.\n                                                                                                       '
        +
        'In in risus lectus. Integer sed laoreet erat, ac commodo eros. Mauris ut ligula at mi feugiat consequat. Proin ultrices viverra commodo. Sed id neque lacus. Suspendisse varius lorem ac posuere porta. Aliquam facilisis pellentesque ante eu porttitor. Nulla vulputate id ipsum et blandit. Donec convallis risus feugiat lobortis finibus. Duis placerat sed lorem in consequat. Pellentesque ante augue, cursus a malesuada quis, luctus vitae nisi. Vestibulum in mattis velit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse mauris ipsum, faucibus in risus vel, porttitor ornare odio. Proin pellentesque scelerisque purus, accumsan porta lorem mattis id.\n                                                                                                             ' +
        'Praesent pellentesque enim eget enim facilisis, et venenatis quam elementum. Vivamus lectus odio, consequat sodales elit sed, mattis tincidunt velit. Mauris sit amet viverra est, quis euismod eros. Fusce dapibus est at laoreet posuere. Quisque et sodales ipsum. Donec condimentum mauris a justo vulputate, ut gravida est commodo. Fusce facilisis pretium lorem ut accumsan. Donec sit amet pulvinar libero. Mauris lobortis vel mi id aliquet. Integer nibh libero, viverra ut condimentum sed, convallis vel ante. Mauris malesuada est a nunc sodales blandit. Integer ultrices ornare rhoncus. Phasellus semper magna sapien. Morbi rhoncus, ipsum non dictum commodo, justo tortor ultrices est, vitae feugiat velit neque ut mauris. Aenean risus tellus, ultrices vel pharetra vitae, eleifend a enim. Curabitur elementum tortor in orci egestas elementum.\n' +
        'Donec ut metus eu magna pharetra congue. Aliquam hendrerit orci a dolor aliquet lacinia. Integer vehicula hendrerit lorem. In eleifend est sit amet mauris cursus, pretium euismod magna aliquet. Aenean id est leo. Etiam nec sem augue. Ut lorem nisi, cursus sit amet dui quis, fringilla dapibus ipsum.',

    //=======================================================
    //========================Polls==========================
    //=======================================================

    CREATE_POLL: 'Create a Poll',
    POLL_SETTING: 'Poll Settings',
    SAVE_DEFAULT_SETTINGS:'Save as default settings \n(except schedule and end date)',
    SAVE_SETTINGS_BUTTON:'Save Settings',

    ADD_IMAGE_HEADER:'Add Image',

    //=================================================
    //===================Profile=======================
    //=================================================

    SAVE_TEXT:'Save',
    MALE_TEXT: "Male",
    FEMALE_TEXT: "Female",
    OTHERS:'Others',
    GENDER_TEXT: "Gender",

    PRIVACY_TEXT:"Privacy",
    PUBLIC_TEXT:"Public",
    PRIVATE_TEXT:"Private",
    CHOOSE_INTEREST:"Choose Interests",
    ADD_MORE_DETAILS:"Add More Details",
    OCCUPATION_TEXT:"Occupation",
    STUDENT_TEXT:"Student",

    RELIGION:"Religion",

    BUDDHIST:"Buddhist",
    CHRISTIAN_TEXT:"Christian",
    HINDU_TEXT:"Hindu",
    JEWISH:"Jewish",
    MUSLIM:"Muslim",
    NON_RELIGIOUS:"Non-religious",
    OTHERS_TEXT:"Other",
    SIKH:"Sikh",

};
export default STRINGS;
