// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Rect} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function UnCheckSVG(props) {
    return (
        <Svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="0.5" y="0.5" width="19" height="19" rx="1.5" stroke={props.fillColor}/>
        </Svg>
    )
}

UnCheckSVG.defaultProps = {
    fillColor: COLORS.app_theme_color,
    width: "24",
    height: "24"
};

export default UnCheckSVG
