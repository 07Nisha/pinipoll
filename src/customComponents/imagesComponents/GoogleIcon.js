// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import Svg, {Rect, Path, G, Defs, ClipPath} from 'react-native-svg';
import COLORS from '../../../src/utils/Colors';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function GoogleIconSVG(props) {
    return (
        <Svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect width="50" height="50" rx="25" fill="#EA4335"/>
            <G clip-path="url(#clip0)">
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M17.6031 25.0001C17.6031 24.1557 17.7432 23.346 17.9937 22.5868L13.6123 19.2411C12.7583 20.9748 12.2773 22.9284 12.2773 25.0001C12.2773 27.0701 12.7579 29.0224 13.6105 30.755L17.9894 27.4029C17.7414 26.647 17.6031 25.8404 17.6031 25.0001Z" fill="white"/>
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M25.2959 17.3182C27.1303 17.3182 28.7872 17.9682 30.089 19.0318L33.8761 15.25C31.5683 13.2409 28.6096 12 25.2959 12C20.1511 12 15.7295 14.9421 13.6123 19.241L17.9935 22.5867C19.003 19.5223 21.8807 17.3182 25.2959 17.3182Z" fill="white"/>
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M25.2959 32.6819C21.8809 32.6819 19.0032 30.4778 17.9937 27.4135L13.6123 30.7585C15.7295 35.0581 20.1511 38.0001 25.2959 38.0001C28.4711 38.0001 31.5027 36.8726 33.7779 34.7601L29.6192 31.545C28.4457 32.2842 26.968 32.6819 25.2959 32.6819Z" fill="white"/>
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M37.7225 25.0002C37.7225 24.2319 37.6041 23.4046 37.4265 22.6366H25.2959V27.6593H32.2785C31.9294 29.3718 30.9791 30.6883 29.6192 31.545L33.778 34.7601C36.168 32.542 37.7225 29.2375 37.7225 25.0002Z" fill="white"/>
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Rect width="26" height="26" fill="white" transform="translate(12 12)"/>
                </ClipPath>
            </Defs>
        </Svg>
    );
}

GoogleIconSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: '24',
    height: '24',
};

export default GoogleIconSVG;
