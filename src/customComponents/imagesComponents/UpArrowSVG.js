// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Rect, Path} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function UpArrowSVG(props) {
    return (
        <Svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M16.7071 5.29289C16.3166 4.90237 15.6834 4.90237 15.2929 5.29289L8.92893 11.6569C8.53841 12.0474 8.53841 12.6805 8.92893 13.0711C9.31946 13.4616 9.95262 13.4616 10.3431 13.0711L16 7.41421L21.6569 13.0711C22.0474 13.4616 22.6805 13.4616 23.0711 13.0711C23.4616 12.6805 23.4616 12.0474 23.0711 11.6569L16.7071 5.29289ZM17 26L17 6L15 6L15 26L17 26Z" fill="white"/>
        </Svg>
    )
}

UpArrowSVG.defaultProps = {
    fillColor: COLORS.app_theme_color,
    width: "24",
    height: "24"
};

export default UpArrowSVG
