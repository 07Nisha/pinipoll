// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
import COLORS from '../../../src/utils/Colors';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function FBIconSVG(props) {
    return (
        <Svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect width="50" height="50" rx="25" fill="white"/>
            <Rect width="50" height="50" rx="25" fill="#3B5998"/>
            <Path d="M26.6667 38V26.4444H30.5185L31.4815 21.6296H26.6667V19.7037C26.6667 17.7778 27.6316 16.8148 29.5556 16.8148H31.4815V12C30.5185 12 29.3244 12 27.6296 12C24.0907 12 21.8519 14.7743 21.8519 18.7407V21.6296H18V26.4444H21.8519V38H26.6667Z" fill="white"/>
        </Svg>
    );
}

FBIconSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: '24',
    height: '24',
};

export default FBIconSVG;
