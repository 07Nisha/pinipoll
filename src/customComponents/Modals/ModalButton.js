// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, TouchableOpacity} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import COLORS from '../../utils/Colors';
import {FONT_FAMILY} from '../../utils/Font';


const ModalButton = (props) => {
    const {label, onButtonPress} = props;
    return (
        <View style={{alignItems: 'center'}}>
            <TouchableOpacity
                onPress={onButtonPress}
                style={{
                    width: wp(85),
                    height: wp(13), alignItems: 'center', justifyContent: 'center',
                    borderRadius: wp(2), borderWidth: 1, borderColor: 'white',
                    backgroundColor: COLORS.app_theme_color,

                }}>
                <Text style={{
                    color: COLORS.white_color,
                    fontSize: wp(4.5),
                    fontFamily: FONT_FAMILY.PoppinsBold,
                }}>{label}</Text>
            </TouchableOpacity>
        </View>
    );
};
export default ModalButton;
