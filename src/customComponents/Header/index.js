// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, StatusBar} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Header} from 'react-native-elements';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------

import style from './styles';
import BackArrow from '../imagesComponents/ArrowBack';
import {FONT_FAMILY} from '../../utils/Font';
import {FONT} from '../../utils/FontSizes';
import COLORS from '../../utils/Colors';
// import LogoutIcon from 'react-native-vector-icons/AntDesign';
// import MenuSVG from '../imagesComponents/MenuSVG';
import SearchIcon from 'react-native-vector-icons/Ionicons';
import {ICONS} from '../../utils/ImagePaths';


const TopHeader = (props) => {
    const {onPressBackArrow, text, isDrawer, image, isRight, isSearchIcon, onRightArrow} = props;

    // return (
    //     <View style={style.mainContainer}>
    //         <View style={style.childContainer}>
    //             <TouchableOpacity
    //                 // style={{paddingTop: wp(4)}}
    //                 onPress={onPressBackArrow}>
    //                 {isDrawer ?
    //                     <Image
    //                         style={{
    //                             width: wp(12),
    //                             height: wp(12),
    //                             borderRadius: wp(12) / 2,
    //                         }}
    //                         source={{uri: image}}
    //                         resizeMode={'contain'}/> : <BackArrow/>}
    //
    //             </TouchableOpacity>
    //             <View style={{alignItems: 'center', width: wp(80)}}>
    //                 {/*  <Image style={{width: wp(75),}} source={ICONS.CHOOSE_PLAN} resizeMode={'contain'}/>*/}
    //
    //                 <Text style={{
    //                     fontFamily: FONT_FAMILY.RobotoBold,
    //                     fontSize: FONT.TextMedium,
    //                     fontStyle: 'normal',
    //                     color: COLORS.app_theme_color,
    //                     // paddingLeft: wp(10),
    //                 }}>{text}</Text>
    //             </View>
    //         </View>
    //     </View>
    // );


    // return (
    //     <Header
    //         backgroundColor={COLORS.backGround_color}
    //         barStyle={'dark-content'}
    //         statusBarProps={{
    //             translucent: true,
    //             backgroundColor: COLORS.transparent,
    //         }}
    //         leftComponent={
    //             <TouchableOpacity onPress={onPressBackArrow}>
    //                 {isDrawer ? <Image
    //                 style={{
    //                     width: wp(12),
    //                     height: wp(12),
    //                     borderRadius: wp(12) / 2,
    //                 }}
    //                 source={{uri: image}}
    //                 resizeMode={'contain'}/> : <BackArrow/>}
    //             </TouchableOpacity>
    //         }
    //         rightComponent={isRight && <TouchableOpacity onPress={onRightArrow}>
    //             <SearchIcon name={'ios-search'} size={wp(8)}
    //                         color={COLORS.black_color}
    //             />
    //         </TouchableOpacity>}
    //         centerComponent={{
    //             text: text,
    //             style: {
    //                 fontFamily: FONT_FAMILY.RobotoBold,
    //                 fontSize: FONT.TextMedium,
    //                 fontStyle: 'normal',
    //                 color: COLORS.app_theme_color,
    //             },
    //         }}
    //         containerStyle={{
    //             borderBottomColor: COLORS.backGround_color,
    //             paddingTop: Platform.OS === 'ios' ? 0 : 25,
    //             height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
    //         }}
    //     />
    // );

    return (
        <Header
            backgroundColor={COLORS.backGround_color}
            barStyle={'dark-content'}
            statusBarProps={{
                translucent: true,
                backgroundColor: COLORS.transparent,
            }}
            leftComponent={
                <TouchableOpacity onPress={onPressBackArrow}>
                    {isDrawer ? <Image
                        style={{
                            width: wp(12),
                            height: wp(12),
                            borderRadius: wp(12) / 2,
                        }}
                        source={{uri: image}}
                        // resizeMode={'contain'}
                    /> : <BackArrow/>}
                </TouchableOpacity>
            }
            rightComponent={isRight && <TouchableOpacity onPress={onRightArrow}>
                {isSearchIcon ?
                    <SearchIcon name={'ios-search'} size={wp(8)}
                                color={COLORS.black_color}
                    />
                    :
                    <Image style={{}} source={ICONS.EDIT_ICON} resizeMode={'contain'}/>
                }

            </TouchableOpacity>}
            centerComponent={{
                text: text,
                style: {
                    fontFamily: FONT_FAMILY.RobotoBold,
                    fontSize: FONT.TextMedium,
                    fontStyle: 'normal',
                    color: COLORS.app_theme_color,
                },
            }}
            containerStyle={{
                borderBottomColor: COLORS.backGround_color,
                paddingTop: Platform.OS === 'ios' ? 0 : 25,
                height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
            }}
        />
    );

};

export default TopHeader;
