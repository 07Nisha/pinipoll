import React from 'react';
import {
    NativeModules,
    TouchableOpacity,
    Platform, View, Image,
} from 'react-native';
import TwitterIconSVG from '../imagesComponents/TwitterIcon';
import InstagramLogin from 'react-native-instagram-login';
import {ICONS} from '../../utils/ImagePaths';

const {RNTwitterSignIn} = NativeModules;

const Constants = {
    //Dev Parse keys
    TWITTER_CONSUMER_KEY: 'YXSuZPRBlR7kSHqUKdAMdCazp',
    TWITTER_CONSUMER_SECRET: 'I7GwKYAMURndWMGDzctHBeRIQLYb5mH6sQXAY2Hg5OaywBlt8n',
};

class InstagramService {

    instagramButton(callback) {
        return (
            <TouchableOpacity onPress={() => {
                this.instagramLogin.show();
            }}>
                <InstagramLogin
                    ref={ref => (this.instagramLogin = ref)}
                    appId='748864152339698'
                    appSecret='f575c2368e5255068a00a5d2aff2560b'
                    redirectUrl='https://socialsizzle.heroku.com/auth/'
                    scopes={['user_profile', 'user_media']}
                    onLoginSuccess={(data) => callback(data)}
                    onLoginFailure={(data) => console.log(data)}
                />
                <Image source={ICONS.INSTAGRAM} resizeMode={'contain'}/>
            </TouchableOpacity>
        );
    }
}

export const instagramService = new InstagramService();
