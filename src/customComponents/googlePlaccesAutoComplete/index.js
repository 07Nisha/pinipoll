import React from "react";
import GooglePlacesAutocomplete from "react-native-google-places-autocomplete";
import BaseClass from "../../utils/BaseClass";
import PropTypes from 'prop-types';
import COLORS from "../../utils/Colors";
import {FONT} from "../../utils/FontSizes";
import {FONT_FAMILY} from "../../utils/Font";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

class AutoCompleteComponent extends BaseClass {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <GooglePlacesAutocomplete
                placeholder={this.props.placeholder}
                minLength={2} // minimum length of text to search
                autoFocus={false}
                fetchDetails={true}
                onPress={(data, details = null) => {
                    this.props.onSelect(data, details)
                }}
                listViewDisplayed={false}
                query={{
                    key: 'AIzaSyDGJ1-GigrEvh2LNBIHSganjJ8w2SHwixA',
                    language: 'en', // language of the results
                    // types: '(cities)', // default: 'geocode'
                }}
                getDefaultValue={() => {
                    return this.props.value
                }}
                styles={{
                    textInputContainer: {
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        borderWidth: 0,
                        backgroundColor: COLORS.TRANSPARENT
                    },
                    textInput: {
                        marginLeft: 0,
                        marginRight: 0,
                        paddingLeft: 0,
                        color: COLORS.light_grey,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.Roboto,
                        backgroundColor: COLORS.TRANSPARENT
                    },
                    description: {
                        color: COLORS.GREY_COLOR,
                        fontSize: FONT.TextSmall,
                        fontFamily: FONT_FAMILY.Roboto
                    },
                    row: {
                        padding: wp(2),
                        verticalPadding:wp(5),
                        height: wp(12),
                        flexDirection: 'row',
                    },
                    listView: {}
                }}
                text={this.props.value}
                nearbyPlacesAPI='GooglePlacesSearch'
                GooglePlacesSearchQuery={{
                    rankby: 'distance',
                    types: 'food',
                }}
                filterReverseGeocodingByTypes={[
                    'locality',
                    'administrative_area_level_3',
                ]}
            />
        );
    }
}

AutoCompleteComponent.propTypes = {
    onSelect: PropTypes.func,
    value: PropTypes.string
};

AutoCompleteComponent.defaultProps = {
    onSelect: () => {
    },
    value: ""
};

export default AutoCompleteComponent;
