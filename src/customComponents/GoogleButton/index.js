import {GoogleSignin, statusCodes} from "@react-native-community/google-signin";
import {TouchableOpacity, Image, View} from 'react-native';
import React from "react";
import GoogleIconSVG from '../imagesComponents/GoogleIcon';

class GoogleService {
    googleLogin (callBack) {
        return (
            <TouchableOpacity onPress={async () => {
                try {
                    await GoogleSignin.hasPlayServices();
                    const userInfo = await GoogleSignin.signIn();
                    callBack(userInfo);
                } catch (error) {
                    callBack(error);
                    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                        console.log('userInfo error1==>', error);
                        // user cancelled the login flow
                    } else if (error.code === statusCodes.IN_PROGRESS) {
                        console.log('userInfo error2==>', error);
                        // operation (f.e. sign in) is in progress already
                    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                        console.log('userInfo error3==>', error);
                        // play services not available or outdated
                    } else {
                        // some other error happened
                        console.log('userInfo error4==>', error);
                    }
                }
            }}>
               <GoogleIconSVG/>
                {/*<Image source={ICONS.GOOGLE_ICON} resizeMode={'contain'}/>*/}
            </TouchableOpacity>
        )

    };
}

export const googleService = new GoogleService();
