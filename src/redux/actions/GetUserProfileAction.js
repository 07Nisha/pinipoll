/**
 * Created by ShipraSharma-React on 10/19/20.
 */

import * as types from '../events/index';
import {API, BASE_URL, DeviceType} from '../constants';
import axios from 'axios';

export const PersonalProfileAction = (payload) => {
    return function (dispatch) {
        let formData = new FormData();
        axios.post(BASE_URL + API.LOGIN , formData, {
            headers:  {
                'Authorization': `${"Bearer" + " " + payload.accessToken}`,
            },
        })
            .then((response) => {
                dispatch(ProfileSuccess(response.data))
            })
            .catch((error) => {
                dispatch(ProfileFail(error))

            }).done();
    }
};


export const ProfileSuccess = (responseData) => {
    return {
        type: types.PROFILE_SUCCESS,
        response: responseData,
    };
};

export const ProfileFail = (error) => {
    return {
        type: types.PROFILE_FAIL,
        error: error.message,
    };
};

