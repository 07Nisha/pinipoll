/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const ForgotPasswordAction = (payload, forgotResponse) => {
    let formData = new FormData();
    formData.append('username', payload.username);
    fetch(BASE_URL + API.FORGOT_PASSWORD, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            forgotResponse(responseData);
        })
        .catch((error) => {
            forgotResponse(error);
        }).done();
};


export const ReSendAction = (payload, resendResponse) => {
    let formData = new FormData();
    formData.append('username', payload.username);
    fetch(BASE_URL + API.RESEND_PIN, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            resendResponse(responseData);
        })
        .catch((error) => {
            resendResponse(error);
        }).done();

};
