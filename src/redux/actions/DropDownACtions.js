/**
 * Created by Nisha-React on 9/26/20.
 */

import {API, BASE_URL} from '../constants';

export const OrganizationTypeAction = (payload, organizationTypeResponse) => {
    let formData = new FormData();
    formData.append('organisation_search', payload.search_text);
    fetch(BASE_URL + API.ORGANIZATION_TYPE, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            organizationTypeResponse(responseData);
        })
        .catch((error) => {
            organizationTypeResponse(error);
        }).done();

};


export const SectorTypeAction = (payload, sectorTypeResponse) => {
    let formData = new FormData();
    formData.append('sector_search', payload.search_text);
    fetch(BASE_URL + API.SECTOR_TYPE, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            sectorTypeResponse(responseData);
        })
        .catch((error) => {
            sectorTypeResponse(error);
        }).done();

};


export const CountryAction = (payload, countryResponse) => {
    let formData = new FormData();
    formData.append('country_name', payload.search_text);
    fetch(BASE_URL + API.COUNTRY, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            countryResponse(responseData);
        })
        .catch((error) => {
            countryResponse(error);
        }).done();

};


export const CityAction = (payload, cityResponse) => {
    let formData = new FormData();
    formData.append('country_name', payload.country_name);
    formData.append('city_name', payload.search_text);

    fetch(BASE_URL + API.CITY, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            cityResponse(responseData);
        })
        .catch((error) => {
            cityResponse(error);
        }).done();

};


export const OccupationAction = (payload, occupationResponse) => {
    let formData = new FormData();
    formData.append('occupation_name', payload.search_text);

    fetch(BASE_URL + API.OCCUPATION, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            occupationResponse(responseData);
        })
        .catch((error) => {
            occupationResponse(error);
        }).done();

};


export const ReligionAction = (payload, religionResponse) => {
    let formData = new FormData();
    formData.append('religions', payload.search_text);

    fetch(BASE_URL + API.RELIGION, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            religionResponse(responseData);
        })
        .catch((error) => {
            religionResponse(error);
        }).done();

};
