/**
 * Created by Shipra-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const GetUserInterests = (payload, chooseInterestResponse) => {

    fetch(BASE_URL + API.CHOOSE_INTEREST, {
        method: 'POST',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.accessToken}`,
            },
    })
        .then((response) => response.json())
        .then((responseData) => {
            chooseInterestResponse(responseData);
        })
        .catch((error) => {
            chooseInterestResponse(error);
        }).done();

};
