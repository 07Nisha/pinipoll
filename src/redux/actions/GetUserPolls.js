import {API, BASE_URL} from '../constants';
import axios from 'axios';

export const GetUserPollAction = (payload, getUserPollResponse) => {
    let formData = new FormData();
    formData.append('user_type', '3');

    axios.post(BASE_URL + API.USER_POLLS, formData, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            getUserPollResponse(response.data);
        })
        .catch((error) => {
            getUserPollResponse(error);

        }).done();
};


export const GetAllPollAction = (payload, getAllPollResponse) => {
    let formData = new FormData();
    formData.append('user_type', '3');

    axios.post(BASE_URL + API.GET_ALL_POLLS, formData, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            getAllPollResponse(response.data);
        })
        .catch((error) => {
            getAllPollResponse(error);

        }).done();
};

export const MostPopularPollAction = (payload, mostPopularPollResponse) => {

    axios.post(BASE_URL + API.MOST_POPULAR_POLL, {}, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            mostPopularPollResponse(response.data);
        })
        .catch((error) => {
            mostPopularPollResponse(error);

        }).done();
};
