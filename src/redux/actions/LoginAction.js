/**
 * Created by Nisha-React on 8/23/20.
 */

import * as types from '../events/index';
import {API, BASE_URL, DeviceType} from '../constants';
import axios from 'axios';

export const LoginAction = (payload) => {
    return function (dispatch) {
        let formData = new FormData();
        formData.append('username', payload.username);
        formData.append('password', payload.password);
        formData.append('device_token', payload.deviceToken);
        formData.append('device_type', DeviceType);
        axios.post(BASE_URL + API.LOGIN , formData, {
            headers:  {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        })
            .then((response) => {
                dispatch(LoginSuccess(response.data))
            })
            .catch((error) => {
                dispatch(LoginFail(error))

            }).done();
    }
};

// export const LoginAction = (payload) => {
//     let formData = new FormData();
//     formData.append('username', payload.username);
//     formData.append('password', payload.password);
//     formData.append('device_token', payload.deviceToken);
//     formData.append('device_type', DeviceType);
//     debugger;
//     return function (dispatch) {
//         fetch(BASE_URL + API.LOGIN, {
//             method: 'POST',
//             headers:
//                 {
//                     'Content-Type': 'application/x-www-form-urlencoded',
//                 },
//             body: formData,
//         })
//             .then((response) => response.json())
//             .then((responseData) => {
//                 dispatch(LoginSuccess(responseData));
//             })
//             .catch((error) => {
//
//                 dispatch(LoginFail(error));
//             }).done();
//     };
// };


export const LoginSuccess = (responseData) => {
    return {
        type: types.LOGIN_SUCCESS,
        response: responseData,
    };
};

export const LoginFail = (error) => {
    return {
        type: types.LOGIN_FAIL,
        error: error.message,
    };
};

