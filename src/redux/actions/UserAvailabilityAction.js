/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const UserAvailabillityAction = (payload, userAvailabillityResponse) => {
    let formData = new FormData();
    formData.append('username', payload.username);
    fetch(BASE_URL + API.CHECK_USER_AVAILABILITY, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            userAvailabillityResponse(responseData);
        })
        .catch((error) => {
            userAvailabillityResponse(error);
        }).done();

};
