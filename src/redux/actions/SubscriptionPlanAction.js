/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const SubscriptionPlanAction = (payload, subscriptionPlanResponse) => {

    fetch(BASE_URL + API.SUBSCRIPTION_PLAN, {
        method: 'POST',
    })
        .then((response) => response.json())
        .then((responseData) => {
            subscriptionPlanResponse(responseData);
        })
        .catch((error) => {
            subscriptionPlanResponse(error);
        }).done();

};

