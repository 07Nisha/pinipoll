/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL, DeviceType} from '../constants';

export const SocialLoginAction = (payload, socialResponse) => {
    let formData = new FormData();
    formData.append('social_id', payload.social_id);
    formData.append('device_token', payload.deviceToken);
    formData.append('device_type', DeviceType);
    fetch(BASE_URL + API.SOCIAL_LOGIN, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            socialResponse(responseData);
        })
        .catch((error) => {
            socialResponse(error);
        }).done();

};
