import React from 'react';
import {
    FlatList,
    Keyboard,
    KeyboardAvoidingView,
    Text,
    TouchableWithoutFeedback,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import BaseClass from '../../../utils/BaseClass';
import TopHeader from '../../../customComponents/Header';
import COLORS from '../../../utils/Colors';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../utils/BaseStyle';
import {FONT} from '../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../utils/Font';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Spacer} from '../../../customComponents/Spacer';
import styles from '../../commonClasses/auth/register/personalRegister/styles';
import CheckSVG from '../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../customComponents/imagesComponents/Uncheck';
import {CategoriesAction} from '../../../redux/actions/CategoriesAction';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../../../utils/Strings';
import OrientationLoadingOverlay from '../../../customComponents/CustomLoader';

export default class Categories extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    componentDidMount(): void {
        AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                this.setState({
                    authToken: token,
                });
                CategoriesAction({accessToken: token}, data => this.categoryResponse(data));
                this.showDialog();
            }
        });
    }

    categoryResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                // this.showToastSucess(message);
                let res = [];

                data.map((item, index) =>
                    res.push({
                        title: item.cat_name,
                        image: item.cat_img,
                        id: item.id,
                        isChecked: false,
                    }),
                );
                this.setState({
                    data: res,
                });

            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    onValueSelect = (item, index) => {
        const {data} = this.state;
        const {navigation, route} = this.props;
        data.map((item1, index1) => {
            if (index === index1) {
                item1.isChecked = true;
            } else {
                item1.isChecked = false;
            }
        });

        this.setState({data: data});
        route.params.onGoBack(item, 'Category');
        navigation.goBack();

    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        const {navigation} = this.props;
        const {data} = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()} text={'Categories'}/>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.backGround_color}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <FlatList
                                    style={{paddingVertical: wp(6)}}
                                    data={data}
                                    keyExtractor={(index) => index}
                                    extraData={this.state}
                                    renderItem={({item, index}) =>
                                        <>
                                            <View style={{
                                                backgroundColor: COLORS.white_color,
                                                width: wp(95),
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                            }}>
                                                <Image
                                                    source={{uri: item.image}}
                                                    style={{
                                                        width: wp(15),
                                                        height: wp(15),
                                                        borderRadius: wp(2.5),
                                                    }}
                                                />
                                                <Spacer row={2}/>
                                                <Text numberOfLines={1} style={{width: wp(65)}}>{item.title}</Text>
                                                <Spacer row={2}/>
                                                <TouchableOpacity
                                                    style={{alignItems: 'flex-end'}}
                                                    onPress={() => this.onValueSelect(item, index)}>
                                                    {item.isChecked ? <CheckSVG/> :
                                                        <UnCheckSVG fillColor={COLORS.black_color}/>}
                                                </TouchableOpacity>

                                            </View>
                                            <Spacer space={2}/>
                                        </>
                                    }/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }

}
