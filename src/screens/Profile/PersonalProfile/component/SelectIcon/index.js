import React, {Component} from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import FastImage from "react-native-fast-image";
//import {COLORS} from '../../../../../utils/Colors';



const SelectIcon = ({item,data, index,onPress}) => {

    return (
        <View style={{
            marginHorizontal: wp(2),
            marginBottom: wp(3),
            alignItems: 'center',
            justifyContent: 'center',

        }}>
            <TouchableOpacity
                onPress={onPress}
            >
                <FastImage
                    style={{width: wp(28), height: wp(28) }}
                    source={{
                        uri: data.avatar_url,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                   // onPress={() => alert("chkkkk")}
                />

               {/* <Image
                    resizeMode='contain'
                    source={{uri: item.avatar_url}}
                    style={{width: wp(28), height: wp(28), }}
                />*/}
            </TouchableOpacity>
        </View>
        // </View>

    );
};
export default SelectIcon;
