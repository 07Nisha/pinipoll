import styled from 'styled-components/native/dist/styled-components.native.esm';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import COLORS from '../../../../utils/Colors';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';


const CreateText = styled.Text`
color: ${COLORS.black_color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.PoppinsBold}; 
`;

const InnerText = styled.Text`
color: ${COLORS.placeHolder_color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.Roboto}; 
`;
const NormalText = styled.Text`
color: ${COLORS.black_color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.Roboto}; 
width:${wp(35)};


`;

const NormalInnerText = styled.Text`
color: ${COLORS.black_color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.Roboto}; 
width:${wp(50)};

`;

export {
    CreateText,
    InnerText,
    NormalText,
    NormalInnerText
};

