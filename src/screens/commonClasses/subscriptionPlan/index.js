import React from "react";
import {
    Image,
    Text,
    Keyboard,
    KeyboardAvoidingView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    FlatList
} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";

import {Input, Header} from 'react-native-elements';
import BaseClass from "../../../utils/BaseClass";
import COLORS from "../../../utils/Colors";
import {ICONS} from '../../../utils/ImagePaths';
import {Spacer} from "../../../customComponents/Spacer";
import {FONT} from "../../../utils/FontSizes";
import {FONT_FAMILY} from '../../../utils/Font';
import {
    MainContainer,
    SafeAreaViewContainer,
    ScrollContainer,

} from "../../../utils/BaseStyle";

import STRINGS from '../../../utils/Strings';
import {PrimaryButton} from '../../../customComponents/ButtonComponent'
import AntDesign from "react-native-vector-icons/AntDesign";
import * as _ from 'lodash';
import TopHeader from "../../../customComponents/Header";
import styles from "../auth/selectAccount/styles";
import RoundTickSVG from "../../../customComponents/imagesComponents/RoundTick";
import RoundSVG from "../../../customComponents/imagesComponents/Round";
// import {SubscriptionPlanAction} from '../../../redux/actions/SubsciptionPlanAction';
// import {RandomAvatarAction} from "../../../redux/actions/RandomAvatarAction";

export default class SubscriptionPlan extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {socialId, socialPlatform, socialEmail} = route.params;
        this.state = {
            image: '',

            /* accountType: [
                 {
                     id: 1,
                     planTitle: 'Analyst Plan',
                     isRadioSelect: false,
                     pollText: 'Post upto',
                     pollText1: '5 polls',
                     pollText2: 'per month',
                     pollText3: 'Free (Limited Offer)',

                 },
                 {
                     id: 2,
                     planTitle: 'Researcher Plan',
                     isRadioSelect: false,
                     // pollText: 'Post upto 10 polls per month \n Free (Limited Offer)'
                     pollText: 'Post upto',
                     pollText1: '10 polls',
                     pollText2: 'per month',
                     pollText3: 'Free (Limited Offer)',
                 },
                 {
                     id: 3,
                     planTitle: 'Investigator Plan',
                     isRadioSelect: false,
                     //  pollText: 'Post upto 25 polls per month \n Free (Limited Offer)'
                     pollText: 'Post upto',
                     pollText1: '25 polls',
                     pollText2: 'per month',
                     pollText3: 'Free (Limited Offer)',
                 },
                 {
                     id: 4,
                     planTitle: 'Scholar Plan',
                     isRadioSelect: false,
                     //pollText: 'Post more than 25 polls per month \n Free (Limited Offer)'
                     pollText: 'Post more than',
                     pollText1: '25 polls',
                     pollText2: 'per month',
                     pollText3: 'Free (Limited Offer)',
                 }
             ],*/
            accountType: [],
            refresh: false,
            isButtonDisabled: true,
            socialId: socialId,
            socialPlatform: socialPlatform,
            socialEmail: socialEmail,

        }
    }


   // componentDidMount() {
    //     const {navigation} = this.props;
    //     this._unsubscribe = navigation.addListener('focus', () => {
    //         this.onFocusFunction();
    //     });
    //
    //
    // }
    //
    // onFocusFunction = () => {
    //     //    this.showDialog();
    //     RandomAvatarAction({}, data => this.isAvatarResponse(data));
    //     //  this.componentDidMount();
    // };
    //
    // componentWillUnmount() {
    //     this._unsubscribe();
    // }

    // isAvatarResponse = (response) => {
    //     if (response !== 'Network request failed') {
    //         //   console.warn("tttt", response)
    //         this.hideDialog();
    //         const {code, message, data} = response;
    //         if (code === '200') {
    //             this.hideDialog();
    //             this.setState(
    //                 {
    //                     image: data[0].avatar_url,
    //                 }
    //             )
    //         } else if (code === '403') {
    //             this.hideDialog();
    //         }
    //     } else {
    //         this.hideDialog();
    //         this.showToastAlert(STRINGS.CHECK_INTERNET);
    //     }
    // };


    /* componentDidMount() {
        // SubscriptionPlanAction({}, data => this.isPlanResponse(data));
     }*/

    /* isPlanResponse = (response) => {
         if (response !== 'Network request failed') {
           //  console.warn("tttt", response)
             this.hideDialog();
             const {code, message, data} = response;
             if (code === '200') {
                 this.hideDialog();
                 //  this.showToastSucess(message)
                 /!*  this.setState({
                       validUName: message,
                   });*!/
             } else if (code === '403') {
                 this.hideDialog();
                 /!* this.setState({
                      validUName: 'Username Unavailable',
                  })*!/
                 ;
             }
         } else {
             this.hideDialog();
             this.showToastAlert(STRINGS.CHECK_INTERNET);
         }
     };


     onRadioPress = (item, index1) => {
         const {accountType} = this.state;
         _.map(accountType, (item, index) => {
             if (index === index1) {
                 return item.isRadioSelect = true;
             } else {
                 return item.isRadioSelect = false;
             }
         });
         this.setState({
             accountType,
             isButtonDisabled: false,
         });
     };*/

    _nextClick() {

        const {navigation} = this.props;
        const {navigate} = navigation;
        const {accountType, socialId, socialPlatform, socialEmail} = this.state;
        navigate('businessSignup', {
            socialId: socialId,
            socialPlatform: socialPlatform,
            socialEmail: socialEmail,
            id: 1,
        });
        //  navigate('businessSignup')
        /*const {accountType, socialId, socialPlatform, socialEmail} = this.state;
        _.map(accountType, (item, index) => {
            switch (index) {
                case 0:
                    if (item.isRadioSelect === true) {
                        navigate('businessSignup', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                            id: item.id
                        });
                    }
                    break;
                case 1:
                    if (item.isRadioSelect === true) {
                        navigate('businessSignup', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                            id: item.id
                        });
                    }
                    break;
                case 2:
                    if (item.isRadioSelect === true) {
                        navigate('businessSignup', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                            id: item.id
                        });
                    }
                    break;
                case 3:
                    if (item.isRadioSelect === true) {
                        navigate('businessSignup', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                            id: item.id
                        });
                    }
                    break;
            }
        });
*/
    }


    render() {
        const {navigation} = this.props
        const {accountType, planList, refresh, isButtonDisabled, image} = this.state;
        return (

            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    //   text={"Choose a Subscription Plan"}
                    onPressBackArrow={() => navigation.goBack()}
                />
                <ScrollContainer>
                    <MainContainer>
                        <Image style={{height: wp(50), width: wp(50)}}
                               source={ICONS.LOGO} resizeMode={'contain'}/>

                        {accountType.length === 0 ?
                            <>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.PoppinsBold,
                                    fontSize: FONT.TextNormal_2,
                                    fontStyle: 'normal',
                                    color: COLORS.app_theme_color,
                                    alignItems: 'center',
                                    textAlign: 'center',
                                }}>{STRINGS.limited_period}</Text>
                                <Spacer space={3}/>
                                <Text  style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontStyle: 'normal',
                                    fontWeight: 'normal',
                                    fontSize: FONT.TextSmall_2,
                                    textAlign: 'center',
                                    color: COLORS.text_Color,
                                    width: wp(80)
                                }}>{STRINGS.initial_text}</Text>
                            </>
                            :

                            _.map(accountType, (item, index) => {
                                    return <>
                                        <View
                                            style={[styles.option_view,
                                                {
                                                    paddingHorizontal: wp(20),
                                                    backgroundColor: item.isRadioSelect ? COLORS.app_theme_color : 'rgba(54,54,57,0.05)'
                                                }]}>
                                            <TouchableOpacity style={{}} onPress={() => this.onRadioPress(item, index)}>
                                                {item.isRadioSelect ? <RoundTickSVG/> : <RoundSVG/>}
                                            </TouchableOpacity>
                                            <Spacer row={2}/>
                                            <View>
                                                <Text style={[styles.option_title,
                                                    {color: item.isRadioSelect ? COLORS.white_color : COLORS.app_theme_color}]}>{item.planTitle}</Text>
                                                <Text
                                                    style={[styles.option_detail,
                                                        {color: item.isRadioSelect ? COLORS.white_color : COLORS.text_Color}]}>{item.pollText}
                                                    <Text
                                                        style={[styles.option_detail1,
                                                            {color: item.isRadioSelect ? COLORS.white_color : COLORS.text_Color}]}> {item.pollText1}
                                                        <Text
                                                            style={[styles.option_detail,
                                                                {color: item.isRadioSelect ? COLORS.white_color : COLORS.text_Color}]}> {item.pollText2}</Text></Text>
                                                </Text>
                                                <Text
                                                    style={[styles.option_detail,
                                                        {color: item.isRadioSelect ? COLORS.white_color : COLORS.text_Color}]}>{item.pollText3}</Text>
                                            </View>

                                        </View>
                                        <Spacer space={2}/>
                                    </>;
                                })
                            }
                        {/*}*/}



                        <Spacer space={5}/>
                        <Image style={{height: hp(20), width: wp(40),}}
                               source={{uri: image}} resizeMode={'contain'}/>

                        <Spacer space={5}/>
                        <PrimaryButton
                            //isDisabled={isButtonDisabled}
                            onPress={() => this._nextClick()}
                            // onPress={() => this.nextClick()}
                            bgColor={COLORS.app_theme_color}
                            //  bgColor={isButtonDisabled ? COLORS.app_theme_color_disabled : COLORS.app_theme_color}
                            text={'Next'}
                            isNextIcon={true}
                        />
                        <Spacer space={3}/>
                    </MainContainer>
                </ScrollContainer>
            </SafeAreaViewContainer>

        )
    }
}
