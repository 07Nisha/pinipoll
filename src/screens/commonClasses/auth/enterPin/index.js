import React from 'react';
import {
    Image,
    Keyboard,
    KeyboardAvoidingView, ScrollView,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import styles from './styles';
import {ICONS} from '../../../../utils/ImagePaths';
import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Input} from 'react-native-elements';
import {VerifyOtp} from '../../../../redux/actions/VerifyPinAction';
import {CommonActions} from '@react-navigation/native';
import TopHeader from '../../../../customComponents/Header';
import RegSuccessModal from '../../../../customComponents/Modals/RegSuccessModal';
import {ReSendAction} from '../../../../redux/actions/ForgotPasswordAction';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';


class EnterPin extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {username, from} = route.params;
        this.state = {
            isVisible: false,
            labelPin: '',
            pin: '',
            areValuesValid: false,
            username: username,
            from: from,
            isLoading: false,
        };
    }


    verifyOtp = () => {
        const {username, pin} = this.state;
        console.warn("username",username)
        VerifyOtp(
            {
                'username': username,
                'otp': pin,
            }, data => this.accountVerifyResponse(data));
        this.showDialog();
    };

    accountVerifyResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;
        console.warn(response, from);
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                if (from === 'Forgot') {
                    navigate('resetPassword', {pin: this.state.pin});
                } else if (from === 'Personal') {
                    this.setState({
                        isVisible: true,
                    });
                }
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    resendPin = () => {
        ReSendAction(
            {username: this.state.username},
            data => this.resendPinResponse(data));
        this.showDialog();
    };


    resendPinResponse = (response) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert('Username does not exist.');
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }

    };

    _goBackToLogin = () => {
        const {navigation} = this.props;
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{name: 'login'}],
            }),
        );
    };

    onChange = async (text) => {
        let arr = [1];
        await arr.map(item => {
            if (text.trim().length === 0) {
                this.setState({
                    labelPin: '',
                });
            } else if (text.trim().length < 4) {
                this.setState({
                    labelPin: 'Enter PIN',
                });
            }
            this.setState({pin: text});
        });

        this.checkAllValuesAdded();
    };

    checkAllValuesAdded = () => {
        const {pin} = this.state;
        if (pin.length < 4) {
            this.setState({areValuesValid: false});
        } else {
            this.setState({areValuesValid: true});
        }
    };

    accept = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{name: 'login'}],
            }),
        );

    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        const {areValuesValid, labelPin, pin, from, isVisible} = this.state;
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                {from === 'Personal' && <TopHeader onPressBackArrow={() => navigation.goBack()}/>}
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollView
                            bounces={false}
                            contentContainerStyle={{
                                flexGrow: 1,
                                justifyContent: 'space-between',
                                flexDirection: 'column',
                            }}>
                            <MainContainer>
                                <Image style={styles.logo_style}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <Text style={styles.main_text}>{STRINGS.enterPin}</Text>
                                <Spacer space={2}/>
                                <Text style={styles.detail_text}>{STRINGS.detail_text_pin}</Text>
                                <Spacer space={7}/>
                                <View style={styles.input_view}>
                                    <Input
                                        maxLength={4}
                                        placeholder='Enter PIN'
                                        label={labelPin}
                                        labelStyle={styles.input_label_style}
                                        inputStyle={styles.input_input_container}
                                        inputContainerStyle={{width: wp(90)}}
                                        value={pin}
                                        keyboardType={'phone-pad'}
                                        onChangeText={(text) => this.onChange(text)}
                                        returnKeyType={'done'}
                                    />
                                </View>
                                <Spacer space={3}/>
                                <PrimaryButton
                                    onPress={() => this.verifyOtp()}
                                    isDisabled={!areValuesValid}
                                    bgColor={areValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    text={from === 'Forgot' ? 'Continue' : STRINGS.verify_btn}/>
                                <Spacer space={3}/>
                                <TouchableOpacity hitSlop={{
                                    right: wp(5),
                                    left: wp(5),
                                    bottom: wp(5),
                                }} onPress={() => {
                                    this.resendPin();
                                }}>
                                    <Text style={styles.resend_text}>{STRINGS.resend_text}</Text>
                                </TouchableOpacity>
                                {from === 'Forgot' && <View style={styles.back_view}>
                                    <Text
                                        onPress={() => this._goBackToLogin()}
                                        style={styles.back_to_login_text}>{STRINGS.back_to_login}</Text>
                                </View>}
                            </MainContainer>
                            <RegSuccessModal
                                accept={() => this.accept()}
                                isVisible={this.state.isLoading ? false : isVisible}
                            />
                            {this._renderCustomLoader()}
                        </ScrollView>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

export default EnterPin;
