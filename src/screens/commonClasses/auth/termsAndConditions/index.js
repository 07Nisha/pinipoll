import React from 'react';

import { View, Text, } from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import BaseClass from '../../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import {Spacer} from "../../../../customComponents/Spacer";
import TopHeader from "../../../../customComponents/Header";
import STRINGS from "../../../../utils/Strings";

class TermsScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            areAllValuesValid: false,
        };
    }

    render() {
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    text={"Terms & Conditions"}
                    onPressBackArrow={() => navigation.goBack()}
                />
                <MainContainer>
                    <ScrollContainer>
                        <View style={{width:wp(85),paddingHorizontal: 2,paddingVertical:wp(3)}}>
                            <Text style={{
                                textAlign:'auto',
                                fontFamily: FONT_FAMILY.Roboto,
                                fontSize: FONT.TextSmall_2,
                                fontStyle: 'normal',
                                color: COLORS.off_black,
                            }}>{STRINGS.TERMS_CONDITIONS_TEXT}</Text>
                        </View>
                        <Spacer space={2}/>
                    </ScrollContainer>
                </MainContainer>

            </SafeAreaViewContainer>

        );
    }
}

export default TermsScreen;
