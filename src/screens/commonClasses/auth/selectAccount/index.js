import React from 'react';

import {Image, View, SafeAreaView, Text, FlatList, TouchableOpacity} from 'react-native';
import * as _ from 'lodash';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import RoundSVG from '../../../../customComponents/imagesComponents/Round';
import RoundTickSVG from '../../../../customComponents/imagesComponents/RoundTick';
import {Spacer} from '../../../../customComponents/Spacer';
import styles from './styles';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';

class SelectAccount extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {socialId, socialPlatform, socialEmail} = route.params;
        this.state = {
            accountType: [
                {
                    id: 1,
                    isRadioSelect: false,
                    title: 'Personal Account',
                    detail: 'For individual users and are free',
                },
                {
                    id: 2,
                    isRadioSelect: false,
                    title: 'Business Account',
                    detail: 'For organisations and may subject to a subscription fee',
                }],
            isButtonDisabled: true,
            socialId: socialId,
            socialPlatform: socialPlatform,
            socialEmail: socialEmail,
        };
    }


    onRadioPress = (item, index1) => {
        const {accountType} = this.state;
        _.map(accountType, (item, index) => {
            if (index === index1) {
                return item.isRadioSelect = true;
            } else {
                return item.isRadioSelect = false;
            }
        });
        this.setState({
            accountType,
            isButtonDisabled: false,
        });
    };

    nextPress = () => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {accountType, socialId, socialPlatform, socialEmail} = this.state;
        _.map(accountType, (item, index) => {
            switch (index) {
                case 0:
                    if (item.isRadioSelect === true) {
                        navigate('register', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                        });
                    }
                    break;
                case 1:
                    if (item.isRadioSelect === true) {
                        navigate('subscriptionPlan', {
                            socialId: socialId,
                            socialPlatform: socialPlatform,
                            socialEmail: socialEmail,
                        });
                    }
                    break;
            }
        });
    };

    render() {
        const {accountType, isButtonDisabled} = this.state;
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()}/>
                <ScrollContainer>
                    <MainContainer
                        // style={{flex: 1, backgroundColor: COLORS.backGround_color, alignItems: 'center'}}
                    >
                        <Image style={{height: wp(50), width: wp(50)}}
                               source={ICONS.LOGO} resizeMode={'contain'}/>
                        <Text style={styles.select_text}>{STRINGS.select_Account}</Text>
                        <Spacer space={0.5}/>
                        <Text style={styles.select_text_details}>{STRINGS.select_account_text}</Text>
                        <Spacer space={5}/>
                        {_.map(accountType, (item, index) => {
                            return <>
                                <View
                                    style={[styles.option_view,
                                        {backgroundColor: item.isRadioSelect ? COLORS.app_theme_color : 'rgba(54,54,57,0.05)'}]}>
                                    <TouchableOpacity onPress={() => this.onRadioPress(item, index)}>
                                        {item.isRadioSelect ? <RoundTickSVG/> : <RoundSVG/>}
                                    </TouchableOpacity>
                                    <Spacer row={2}/>
                                    <View>
                                        <Text style={[styles.option_title,
                                            {color: item.isRadioSelect ? COLORS.white_color : COLORS.app_theme_color}]}>{item.title}</Text>
                                        <Text numberOfLines={2}
                                              style={[styles.option_detail,
                                                  {color: item.isRadioSelect ? COLORS.white_color : COLORS.text_Color}]}>{item.detail}</Text>
                                    </View>
                                </View>
                                <Spacer space={2}/>
                            </>;
                        })
                        }
                        <Spacer space={2}/>
                        <PrimaryButton
                            isDisabled={isButtonDisabled}
                            onPress={() => this.nextPress()}
                            bgColor={isButtonDisabled ? COLORS.app_theme_color_disabled : COLORS.app_theme_color}
                            text={'Next'}
                            isNextIcon={true}
                        />
                    </MainContainer>
                </ScrollContainer>
            </SafeAreaViewContainer>
        );
    }
}

export default SelectAccount;
