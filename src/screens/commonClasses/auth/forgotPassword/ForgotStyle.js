import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import {FONT_FAMILY} from '../../../../utils/Font';
import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    keyboard_style: {
        flex: 1,
        backgroundColor: COLORS.app_theme_color_disabled,
    },
    header_text: {
        fontFamily: FONT_FAMILY.PoppinsBold,
        fontSize: FONT.TextNormal,
        fontStyle: 'normal',
        lineHeight: wp(10),
        color: COLORS.app_theme_color,
        alignItems: 'center',
        textAlign: 'center',
    },
    detail_text: {
        width: wp(90),
        fontFamily: FONT_FAMILY.Roboto,
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: FONT.TextSmall_2,
        textAlign: 'center',
        color: COLORS.off_black,
    },
    input_label_style: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall,
        color: COLORS.black_color,
    },
    input_error_style: {
        color: COLORS.invalid_color,
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
    },
    input_input_container: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextMedium_2,
        color: COLORS.black_color,
    },
    back_view: {
        flex:1,
        justifyContent: 'flex-end',
        marginBottom: wp(10),
    },
    back_to_login_text: {
        fontFamily: FONT_FAMILY.Poppins,
        fontSize: FONT.TextSmall_2,
        color: COLORS.app_theme_color,
        fontWeight: 'bold',
    },
});

export default styles;
