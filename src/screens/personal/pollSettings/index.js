import React from 'react';
import {
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import BaseClass from '../../../utils/BaseClass';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../utils/BaseStyle';
import TopHeader from '../../../customComponents/Header';
import COLORS from '../../../utils/Colors';
import {FONT} from '../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../utils/Font';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Spacer} from '../../../customComponents/Spacer';
import STRINGS from '../../../utils/Strings';
import ModalDropdown from '../../../customComponents/ModalDropdown';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Input} from 'react-native-elements';
import styles from '../../commonClasses/auth/register/personalRegister/styles';
import moment from 'moment';
import CheckSVG from '../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../customComponents/imagesComponents/Uncheck';
import {PrimaryButton} from '../../../customComponents/ButtonComponent';

const VOTE_PRIVACY = ['Everyone (Public)', 'My Friends (Private)', 'My Groups (Private)'];
const POST_TIME = ['Post Now', 'Schedule'];
const VOTE_END_DATE = ['Until a particular date', 'For no. of votes and date (whatever ends first)'];
const RESULT_VISIBLE_TO = ['Only invited people', 'Everyone'];
const SHOW_RESULT = ['Show result permanently', 'Hide result after 7 days'];

export default class PollSettings extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            votesFrom: 'Everyone (Public)',
            labelFrom: 'Allow Votes From',
            voteFromId: 1,

            liveFrom: 'Post Now',
            labelLive: 'Poll Live from',
            liveFromId: 1,

            voteEndDate: 'Until a particular date',
            labelVoteEndDate: 'Poll end type',
            voteEndDateId: 1,

            resultVisibleTo: 'Only invited people',
            labelResultVisibleTo: 'Poll result visible to',
            resultVisibleToId: 1,

            showResult: 'Show result permanently',
            labelShowResult: 'Poll results',
            showResultId: 1,

            labelDate: '',
            validDate: 'This field can not be empty',
            datePicker: new Date(),
            datePicker1: moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
            datePicker2: new Date(),
            showPicker: false,

            labelUntilDate: '',
            validUntilDate: 'This field can not be empty',
            untilDatePicker: new Date(),
            untilDatePicker1: new Date(),
            showUntilPicker: false,

            labelVoteNumber: '',
            voteNumber: 0,
            validVoteNumber: '',

            checked1: false,
            checked2: false,

            areAllValuesValid: false,

            changeDate: false,


        };
        this.isPickerShowing = false;
        this.isUntilParDate = false;
    }


    hideDatePicker = () => {
        this.isPickerShowing = false;
        this.setState({
            showPicker: false,
        });
    };

    hideUntilDatePicker = () => {
        this.isUntilParDate = false;
        this.setState({
            showUntilPicker: false,
        });
    };

    onSettingClick = () => {
        const {navigation, route} = this.props;
        const {
            voteFromId, liveFromId, voteEndDateId, resultVisibleToId, showResultId,
            datePicker1, untilDatePicker1, voteNumber, checked1,
        } = this.state;
        let data = {
            'voteFrom': voteFromId,
            'pollResultVisibleTo': resultVisibleToId,
            'pollResult': showResultId,
            'makeAnonymous': checked1,
            'startDate': datePicker1,
            'endDate': untilDatePicker1,
            'numberOfVotes': voteNumber,
        };

        route.params.onGoBack(data, 'Settings');
        navigation.goBack();
    };


    dateChange = () => {
        console.warn('datePicker date change');
        this.setState({changeDate: true});
    };

    onChange = async (text, index, type) => {
        const {checked1, checked2, datePicker2, changeDate, liveFromId} = this.state;
        console.warn('datePicker', changeDate);
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'voteFrom':
                    if (text.length === 0) {
                        this.setState({
                            labelFrom: '',
                        });
                    } else {
                        this.setState({
                            labelFrom: 'Allow votes from',
                        });
                    }
                    if (index === '0') {
                        this.setState({
                            votesFrom: text,
                            voteFromId: 1,
                        });
                    } else if (index === '1') {
                        this.setState({
                            votesFrom: text,
                            voteFromId: 2,
                        });
                    } else if (index === '2') {
                        this.setState({
                            votesFrom: text,
                            voteFromId: 3,
                        });
                    }

                    break;

                case 'voteLive':
                    if (text.length === 0) {
                        this.setState({
                            labelLive: '',
                        });
                    } else {
                        this.setState({
                            labelLive: 'Poll live from',
                        });
                    }

                    if (index === '0') {
                        this.setState({
                            liveFrom: text,
                            liveFromId: 1,
                        });
                    } else if (index === '1') {
                        this.setState({
                            liveFrom: text,
                            liveFromId: 2,
                        });
                    }
                    break;

                case 'scheduleDateTime':
                    this.hideDatePicker();

                    let dateValue = moment(text).format('DD MM YYYY hh:mm A');
                    let dateNew = moment(new Date()).format('DD MM YYYY hh:mm A');

                    if (text !== '') {
                        this.setState({
                            labelDate: 'Scheduled Date and Time',
                            validDate: '',
                        });
                    }
                    if (dateNew === dateValue) {
                        this.setState({
                            validDate: 'Rather you can choose POST NOW if you want to select current Time',
                        });
                    } else {
                        this.setState({
                            validDate: '',
                        });
                    }
                    let dateSel = moment(text).format('DD MMM YYYY, hh:mm A');
                    let dateSel2 = moment(text).format('YYYY-MM-DD hh:mm:ss');
                    this.setState({
                        datePicker: dateSel,
                        datePicker1: dateSel2,
                        datePicker2: text,
                        untilDatePicker: new Date(),
                        untilDatePicker1: new Date(),
                    });
                    break;

                case 'voteEndDate':
                    if (text.length === 0) {
                        this.setState({
                            labelVoteEndDate: '',
                        });
                    } else {
                        this.setState({
                            labelVoteEndDate: 'Poll end type',
                        });
                    }

                    if (index === '0') {
                        this.setState({
                            voteEndDate: text,
                            voteEndDateId: 1,
                            labelUntilDate: '',
                            untilDatePicker: new Date(),
                            labelVoteNumber: '',
                            voteNumber: 0,
                            validUntilDate: 'This field can not be empty',
                        });
                    } else if (index === '1') {
                        this.setState({
                            voteEndDate: text,
                            voteEndDateId: 2,
                            labelUntilDate: '',
                            untilDatePicker: new Date(),
                            labelVoteNumber: '',
                            voteNumber: 0,
                            validUntilDate: 'This field can not be empty',
                        });
                    }
                    break;

                case 'endDateTime':
                    this.hideUntilDatePicker();
                    if (Platform.OS === 'ios') {
                        if (changeDate === false) {
                            let currDate = new Date(datePicker2);
                            // currDate = currDate.setDate(currDate.getDate() + 1);
                            var fulldate = new Date(currDate); //--> your minimum date

                            let dateValue1 = moment(fulldate).format('DD MM YYYY hh:mm A');
                            let dateNew1 = moment(new Date()).format('DD MM YYYY hh:mm A');
                            if (text !== '') {
                                this.setState({
                                    labelUntilDate: 'Select Date and Time',
                                    validUntilDate: '',
                                });
                            }
                            if (dateNew1 === dateValue1) {
                                this.setState({
                                    validUntilDate: 'End date time field can not be current Time',
                                });
                            } else {
                                this.setState({
                                    validUntilDate: '',
                                });
                            }

                            this.setState({
                                changeDate: false,
                                untilDatePicker: moment(fulldate, 'DD MMM YYYY, hh:mm A').format('DD MMM YYYY, hh:mm A'),
                                untilDatePicker1: moment(fulldate, 'YYYY-MM-DD hh:mm:ss').format('YYYY-MM-DD hh:mm:ss'),
                            });
                        } else {
                            let dateValue1 = moment(text).format('DD MM YYYY hh:mm A');
                            let dateNew1 = moment(new Date()).format('DD MM YYYY hh:mm A');
                            if (text !== '') {
                                this.setState({
                                    labelUntilDate: 'Select Date and Time',
                                    validUntilDate: '',
                                });
                            }
                            if (dateNew1 === dateValue1) {
                                this.setState({
                                    validUntilDate: 'End date time field can not be current Time',
                                });
                            } else {
                                this.setState({
                                    validUntilDate: '',
                                });
                            }
                            let dateSelected = moment(text).format('DD MMM YYYY, hh:mm A');
                            let dateSelected2 = moment(text).format('YYYY-MM-DD hh:mm:ss');
                            this.setState({
                                untilDatePicker: dateSelected,
                                untilDatePicker1: dateSelected2,
                            });
                        }
                    } else {
                        let currDate = new Date(datePicker2);
                        var fulldate = new Date(currDate); //--> your minimum date

                        let dateValue1 = moment(fulldate);
                        let dateNew1 = moment(text);

                        if (text !== '') {
                            this.setState({
                                labelUntilDate: 'Select Date and Time',
                                validUntilDate: '',
                            });
                        }
                        if (Date.parse(dateValue1) > Date.parse(dateNew1)) {
                            this.setState({
                                validUntilDate: 'End date time field can not be less than start time',
                            });
                        } else {
                            this.setState({
                                validUntilDate: '',
                            });
                        }
                        let dateSelected = moment(text).format('DD MMM YYYY, hh:mm A');
                        let dateSelected2 = moment(text).format('YYYY-MM-DD hh:mm:ss');
                        this.setState({
                            untilDatePicker: dateSelected,
                            untilDatePicker1: dateSelected2,
                        });
                    }
                    break;

                case 'numberOfVotes':
                    if (text.length === 0) {
                        this.setState({
                            labelVoteNumber: '',
                            validVoteNumber: 'Field can not be empty.',
                        });
                    } else {
                        this.setState({
                            labelVoteNumber: 'No. of Votes',
                            validVoteNumber: '',
                        });
                    }

                    this.setState({
                        voteNumber: text,
                    });
                    break;

                case 'resultVisibleTo':
                    if (text.length === 0) {
                        this.setState({
                            labelResultVisibleTo: '',
                        });
                    } else {
                        this.setState({
                            labelResultVisibleTo: 'Poll result visible to',
                        });
                    }

                    if (index === '0') {
                        this.setState({
                            resultVisibleTo: text,
                            resultVisibleToId: 1,
                        });
                    } else if (index === '1') {
                        this.setState({
                            resultVisibleTo: text,
                            resultVisibleToId: 2,
                        });
                    }
                    break;

                case 'showResult':
                    if (text.length === 0) {
                        this.setState({
                            labelShowResult: '',
                        });
                    } else {
                        this.setState({
                            labelShowResult: 'Poll results',
                        });
                    }

                    if (index === '0') {
                        this.setState({
                            showResult: text,
                            showResultId: 1,
                        });
                    } else if (index === '1') {
                        this.setState({
                            showResult: text,
                            showResultId: '2',
                        });
                    }
                    break;

                case 'terms1':
                    this.setState({checked1: !checked1});
                    break;
                case 'terms2':
                    this.setState({checked2: !checked2});
                    break;
            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {validUntilDate, validDate, validVoteNumber, voteNumber, voteEndDateId, liveFromId} = this.state;
        if (liveFromId === 2) {
            if (validDate !== '') {
                this.setState({
                    areAllValuesValid: false,
                });
            } else {
                this.setState({areAllValuesValid: true});
            }
        }
        if (voteEndDateId === 1) {
            if (validUntilDate !== '') {
                this.setState({
                    areAllValuesValid: false,
                });
            } else {
                this.setState({areAllValuesValid: true});
            }
        }
        if (voteEndDateId === 2) {
            if (validVoteNumber !== '' || voteNumber === 0 || validUntilDate !== '') {
                this.setState({
                    areAllValuesValid: false,
                });
            } else {
                this.setState({areAllValuesValid: true});
            }
        }
    };


    _renderDropDowns = () => {
        const {
            labelFrom, votesFrom, labelLive, liveFrom, liveFromId, labelVoteEndDate, voteEndDate, voteEndDateId, labelResultVisibleTo, resultVisibleTo,
            labelShowResult, showResult, datePicker, labelDate, validDate, labelUntilDate, untilDatePicker, validUntilDate, labelVoteNumber, voteNumber, validVoteNumber, checked1, checked2,
        } = this.state;

        console.warn(showResult, checked1, checked2);
        return (
            <View style={{width: wp(90)}}>
                {labelFrom.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    paddingBottom: wp(3),
                }}>{'Allow votes from'}</Text>}
                <ModalDropdown
                    defaultValue={votesFrom}
                    style={{
                        width: wp(90),
                        borderBottomWidth: wp(0.3),
                        borderBottomColor: COLORS.arrow_color,
                    }}
                    textStyle={{
                        paddingBottom: wp(3),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    dropdownStyle={{width: wp(90)}}
                    dropdownTextStyle={{
                        paddingVertical: wp(2),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    options={VOTE_PRIVACY}
                    onSelect={(label, value) => this.onChange(value, label, 'voteFrom')}
                />
                <Spacer space={3}/>

                {labelLive.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    paddingBottom: wp(3),
                }}>{'Poll live from'}</Text>}
                <ModalDropdown
                    defaultValue={liveFrom}
                    style={{
                        width: wp(90),
                        borderBottomWidth: wp(0.3),
                        borderBottomColor: COLORS.arrow_color,
                    }}
                    textStyle={{
                        paddingBottom: wp(3),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    dropdownStyle={{width: wp(90), height: wp(30)}}
                    dropdownTextStyle={{
                        paddingVertical: wp(2),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    options={POST_TIME}
                    onSelect={(label, value) => this.onChange(value, label, 'voteLive')}
                />
                <Spacer space={3}/>

                {liveFromId === 2 &&
                <View style={{width: wp(90), justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => {
                        this.isPickerShowing = true;
                        this.setState({showPicker: true});
                    }}>
                        <Input
                            pointerEvents={'none'}
                            editable={false}
                            placeholder='Select scheduled Date and Time'
                            label={labelDate}
                            value={datePicker}
                            errorStyle={styles.input_error_style}
                            errorMessage={validDate === 'This field can not be empty' ? '' : validDate}
                            labelStyle={{
                                fontFamily: FONT_FAMILY.RobotoSemiBold,
                                fontSize: FONT.TextSmall_2,
                                color: COLORS.off_black,
                                paddingBottom: wp(3),
                            }}
                            inputStyle={styles.input_input_container}
                            inputContainerStyle={{width: wp(90)}}
                            // onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                        />
                    </TouchableOpacity>
                    {/*<Spacer space={3}/>*/}
                </View>
                }


                {labelVoteEndDate.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    paddingBottom: wp(3),
                }}>{'Poll end type'}</Text>}
                <ModalDropdown
                    defaultValue={voteEndDate}
                    style={{
                        width: wp(90),
                        borderBottomWidth: wp(0.3),
                        borderBottomColor: COLORS.arrow_color,
                    }}
                    textStyle={{
                        paddingBottom: wp(3),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                        width: wp(81.5),
                    }}
                    dropdownStyle={{width: wp(90), height: wp(30)}}
                    dropdownTextStyle={{
                        paddingVertical: wp(2),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    options={VOTE_END_DATE}
                    onSelect={(label, value) => this.onChange(value, label, 'voteEndDate')}
                />
                <Spacer space={3}/>

                {voteEndDateId === 1 ?
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => {
                            this.isUntilParDate = true;
                            this.setState({showUntilPicker: true});
                        }}>
                            <Input
                                pointerEvents={'none'}
                                editable={false}
                                placeholder='Select End Date and Time'
                                label={labelUntilDate}
                                value={untilDatePicker}
                                errorStyle={styles.input_error_style}
                                errorMessage={validUntilDate === 'This field can not be empty' ? '' : validUntilDate}
                                labelStyle={{
                                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.off_black,
                                    paddingBottom: wp(3),
                                }}
                                inputStyle={styles.input_input_container}
                                inputContainerStyle={{width: wp(90)}}
                            />
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{
                        width: wp(90),
                        flexDirection: 'row',
                    }}>
                        <View style={{width: wp(35)}}>
                            <Input
                                containerStyle={{paddingLeft: 0}}
                                placeholder='No. of Votes'
                                label={labelVoteNumber}
                                keyboardType={'phone-pad'}
                                labelStyle={{
                                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.off_black,
                                    paddingBottom: wp(3),
                                }}
                                errorStyle={styles.input_error_style}
                                errorMessage={validVoteNumber}
                                inputStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    marginLeft: 0,
                                    color: COLORS.black_color,
                                }}
                                inputContainerStyle={{width: wp(30)}}
                                value={voteNumber}
                                onChangeText={(text) => this.onChange(text, 0, 'numberOfVotes')}
                                returnKeyType={'done'}
                            />
                        </View>
                        <View style={{width: wp(55)}}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.isUntilParDate = true;
                                    this.setState({showUntilPicker: true});
                                }}>
                                <Input
                                    containerStyle={{paddingLeft: 0}}
                                    pointerEvents={'none'}
                                    editable={false}
                                    placeholder='Select End Date and Time'
                                    label={labelUntilDate}
                                    value={untilDatePicker}
                                    errorStyle={styles.input_error_style}
                                    errorMessage={validUntilDate === 'This field can not be empty' ? '' : validUntilDate}
                                    labelStyle={{
                                        fontFamily: FONT_FAMILY.RobotoSemiBold,
                                        fontSize: FONT.TextSmall_2,
                                        color: COLORS.off_black,
                                        paddingBottom: wp(3),
                                    }}
                                    inputStyle={styles.input_input_container}
                                    inputContainerStyle={{width: wp(55)}}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                }

                {labelResultVisibleTo.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    paddingBottom: wp(3),
                }}>{'Poll result visible to'}</Text>}
                <ModalDropdown
                    defaultValue={resultVisibleTo}
                    style={{
                        width: wp(90),
                        borderBottomWidth: wp(0.3),
                        borderBottomColor: COLORS.arrow_color,
                    }}
                    textStyle={{
                        paddingBottom: wp(3),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    dropdownStyle={{width: wp(90), height: wp(30)}}
                    dropdownTextStyle={{
                        paddingVertical: wp(2),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    options={RESULT_VISIBLE_TO}
                    onSelect={(label, value) => this.onChange(value, label, 'resultVisibleTo')}
                />
                <Spacer space={3}/>

                {labelShowResult.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    paddingBottom: wp(3),
                }}>{'Poll results'}</Text>}
                <ModalDropdown
                    defaultValue={showResult}
                    style={{
                        width: wp(90),
                        borderBottomWidth: wp(0.3),
                        borderBottomColor: COLORS.arrow_color,
                    }}
                    textStyle={{
                        paddingBottom: wp(3),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    dropdownStyle={{width: wp(90), height: wp(30)}}
                    dropdownTextStyle={{
                        paddingVertical: wp(2),
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    options={SHOW_RESULT}
                    onSelect={(label, value) => this.onChange(value, label, 'showResult')}
                />

                <Spacer space={4}/>
                <TouchableOpacity
                    style={styles.check_box_touchable}
                    onPress={() => this.onChange('', 0, 'terms1')}>
                    {checked1 ? <CheckSVG/> : <UnCheckSVG/>}
                    <Spacer row={2}/>
                    <Text style={{
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                        fontFamily: FONT_FAMILY.Roboto,
                    }}>Make poll anonymous</Text>
                </TouchableOpacity>

                <Spacer space={4}/>
                <TouchableOpacity
                    style={{
                        width: wp(90),
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                    onPress={() => this.onChange('', 0, 'terms2')}>
                    {checked2 ? <CheckSVG/> : <UnCheckSVG/>}
                    <Spacer row={2}/>
                    <Text style={{
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                        fontFamily: FONT_FAMILY.Roboto,
                    }}>{STRINGS.SAVE_DEFAULT_SETTINGS}</Text>
                </TouchableOpacity>

            </View>
        );
    };

    render() {
        const {showPicker, datePicker, datePicker2, showUntilPicker, untilDatePicker, areAllValuesValid, liveFromId} = this.state;
        const {navigation} = this.props;
        let currDate = new Date(datePicker2);
        // currDate = currDate.setDate(currDate.getDate() + 1);
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()} text={STRINGS.POLL_SETTING}/>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.backGround_color}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Spacer space={5}/>
                                {this._renderDropDowns()}

                                {this.isPickerShowing && <DateTimePickerModal
                                    isVisible={showPicker}
                                    date={new Date(datePicker)}
                                    mode="datetime"
                                    minimumDate={new Date()}
                                    onConfirm={(date) => this.onChange(date, 0, 'scheduleDateTime')}
                                    onCancel={() => this.hideDatePicker()}
                                />}
                                {this.isUntilParDate && <DateTimePickerModal
                                    isVisible={showUntilPicker}
                                    date={new Date(untilDatePicker)}
                                    mode="datetime"
                                    minimumDate={currDate}
                                    onChange={(date) => this.dateChange(date)}
                                    onConfirm={(date) => {
                                        console.warn('date', date);
                                        this.onChange(date, 0, 'endDateTime');
                                    }}
                                    onCancel={() => this.hideUntilDatePicker()}
                                />}

                                <Spacer space={5}/>

                                <PrimaryButton onPress={() => this.onSettingClick()}
                                               text={STRINGS.SAVE_SETTINGS_BUTTON}
                                               isDisabled={!areAllValuesValid}
                                               bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                />
                                <Spacer space={5}/>
                            </MainContainer>
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
};
