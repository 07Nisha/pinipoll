import React from 'react';

import {Image, View, SafeAreaView, Text, TextInput, Alert} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../utils/ImagePaths';
import BaseClass from '../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../utils/Font';
import {FONT} from '../../../utils/FontSizes';
import COLORS from '../../../utils/Colors';

import {MainContainer, SafeAreaViewContainer, ScrollContainer, ShadowViewContainer} from '../../../utils/BaseStyle';
import {Spacer} from '../../../customComponents/Spacer';
import TopHeader from '../../../customComponents/Header';
import STRINGS from '../../../utils/Strings';
import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';
import OrientationLoadingOverlay from '../../../customComponents/CustomLoader';
import AsyncStorage from '@react-native-community/async-storage';
import {LogoutAction} from '../../../redux/actions/LogoutAction';
import HomeTopTab from './homeTopTabBar';
import {GetUserPollAction} from '../../../redux/actions/GetUserPolls';
// import SearchIcon from 'react-native-vector-icons/Ionicons';
// import CrossIcon from 'react-native-vector-icons/Entypo';

export default class Home extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        this.state = {
            isLogOut: false,
            authToken: '',
            searchText: '',
            profile: 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
        };
    }

    componentDidMount() {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
    }

    onFocusFunction = () => {
        AsyncStorage.getItem(STRINGS.userPersonalInfo, async (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                await this.setState({
                    loginData: loginData,
                    profile: loginData.profile_image !== null ? loginData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
                    userName: `${loginData.firstname + ' ' + loginData.lastname}`,
                    account_type: loginData.account_type === '1' ? 'Individual Account' : loginData.account_type === '2' ? 'Business Account' : '',
                    account_type_id: loginData.account_type,

                });
            }
        });
    };

    componentWillUnmount() {
        this._unsubscribe();
    }


    render() {
        const {navigation} = this.props;
        const {searchText, profile} = this.state;
        return (
            <SafeAreaViewContainer>
                {/*<TopHeader isLogout={true} onPressBackArrow={() => this.logoutUser()}/>*/}
                <TopHeader isDrawer={true}
                           text={'Home'}
                           image={profile}
                           isRight={true}
                           isSearchIcon={true}
                           onRightArrow={() => this.showToastAlert(STRINGS.COMING_SOON)}
                           onPressBackArrow={() => navigation.openDrawer()}/>
                <View style={{backgroundColor: COLORS.backGround_color, flex: 1}}>
                    <HomeTopTab props={this.props}/>
                </View>
            </SafeAreaViewContainer>

        );
    }
}
